"use client";

import signUp from "@apps/api/signIn";
import { useRouter } from 'next/navigation';
import { SubmitHandler, useForm } from "react-hook-form";

type Inputs = {
  email: string,
  password: string,
};
export default function page() {
  const router = useRouter()
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs>  =  async ({email, password}) => {
    const { result, error } = await signUp(email, password);


    if (error) {
      return console.log(error)
  }
  return router.push("/game")
  }
  return (
    <main className="flex min-h-screen flex-col items-center justify-center p-24">
      <form className="grid grid-cols-1 items-center gap-4" onSubmit={handleSubmit(onSubmit)}>
        <input
          className="input w-full max-w-xs"
          type="email"
          placeholder="email@user.com"
          {...register("email")}
        />

        <input
        className="input w-full max-w-xs"
        placeholder="****"
        {...register("password", { required: true, minLength: 6 })} />
        {errors.password?.type === "required" && <span className="text-red-500">This field is required</span>}
        {errors.password?.type === "minLength" && <span className="text-red-500">Password must be at least 6 characters</span>}
        <button className="btn btn-primary" type="submit" > crear usuario </button>
      </form>
    </main>
  );
}
