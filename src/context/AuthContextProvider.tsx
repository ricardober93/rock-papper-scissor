import { auth } from "@apps/api/config/firebase";
import { onAuthStateChanged } from "firebase/auth";
import { createContext, useContext, useEffect, useState } from "react";
import { User as FirebaseUser } from "firebase/auth";

export type authContextType = {
    user: FirebaseUser | null;
};

export const AuthContext = createContext<authContextType>({
    user: null,
});

export const useAuthContext = () => useContext(AuthContext);

export const AuthContextProvider = ({
    children,
}: { children: React.ReactNode }) => {
    const [user, setUser] = useState<FirebaseUser | null>(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                setUser(user);
            } else {
                setUser(null);
            }
            setLoading(false);
        });

        return () => unsubscribe();
    }, []);

    return (
        <AuthContext.Provider value={{ user }}>
            {loading ? <div>Loading...</div> : children}
        </AuthContext.Provider>
    );
};
