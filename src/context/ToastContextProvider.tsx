import React, { createContext, useContext } from "react";
import { toast, ToastContainer, ToastOptions } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

type ToastFunction = (message: string, options?: ToastOptions) => void;

interface ToastContextType {
  success: ToastFunction;
  error: ToastFunction;
  warning: ToastFunction;
  info: ToastFunction;
}

const ToastContext = createContext<ToastContextType | undefined>(undefined);

export function ToastProvider({ children }: { children: React.ReactNode }) {
  const toastFunctions: ToastContextType = {
    success: (message, options) => toast.success(message, options),
    error: (message, options) => toast.error(message, options),
    warning: (message, options) => toast.warn(message, options),
    info: (message, options) => toast.info(message, options),
  };
  return (
    <ToastContext.Provider value={toastFunctions}>
      {children}
      <ToastContainer
        position="top-right"
        autoClose={3000}
        draggable
      />
    </ToastContext.Provider>
  );
}

export const useToast = (): ToastContextType => {
  const toastFunctions = useContext(ToastContext);

  if (!toastFunctions) {
    throw new Error("useToast must be used within a ToastProvider");
  }

  return toastFunctions;
};
