import { getApps, initializeApp } from "firebase/app";
import "firebase/auth";
import { getAuth } from "firebase/auth";
import "firebase/firestore";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDOzLcIM2KlNpTjsJKTkJ85pW0f9QlO4Fk",
  authDomain: "rock-paper-sicssors-app.firebaseapp.com",
  projectId: "rock-paper-sicssors-app",
  storageBucket: "rock-paper-sicssors-app.appspot.com",
  messagingSenderId: "276944356381",
  appId: "1:276944356381:web:1847ae7b22dfa932d532f4",
  measurementId: "G-CD91L15CED",
};

// Initialize Firebase
let firebase_app = getApps().length === 0 ? initializeApp(firebaseConfig) : getApps()[0];

export const auth = getAuth(firebase_app);
export const db = getFirestore(firebase_app)
export default firebase_app;
