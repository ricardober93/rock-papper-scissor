import useLogoutFirebase from "@apps/api/logout";
import { useToast } from "@apps/context/ToastContextProvider";
import Link from "next/link";
import { useRouter } from "next/navigation";

type Props = {
    photoUrl: string
}

export default function Header({photoUrl}: Props) {
  const router = useRouter();
  const toast = useToast();
  const {  logOut  } = useLogoutFirebase();

  const handleLogOut = () => {
    logOut();
    toast.success("Has cerrado sesión");
    return router.push("/login")
  }

  return (
    <div className="navbar bg-base-100">
    <div className="flex-1">
      <Link href="/" className="btn btn-ghost normal-case text-xl">Rock - Paper - Scissors</Link>
    </div>
    <div className="flex-none gap-2">
      <div className="dropdown dropdown-end">
        <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
          <div className="w-10 rounded-full">
            <img src={photoUrl ?? '/images/user-placeholder.png'} />
          </div>
        </label>
        <ul tabIndex={0} className="mt-3 p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52">
          <li onClick={handleLogOut} ><a>Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  )
}