import { doc, setDoc } from "firebase/firestore";
import { db } from "./config/firebase";
import { namingCollection } from "./names-collection";

const createRoom = async (creatorId: string, roomName: string) => {
  try {
    // Generate a unique roomId
    const roomId = crypto.randomUUID(); // You can use any unique ID generation mechanism

    // Create the room document in Firestore
    await setDoc(doc(db, namingCollection.rooms, roomId), {
      roomId,
      participants: [creatorId],
      roomName,
      status: "active",
    });

    return roomId;
  } catch (error) {
    console.error("Error creating room:", error);
    throw new Error("Failed to create room");
  }
};