'use client';
import { confetti } from "tsparticles-confetti";

export const Showconfetti = async () => {
    await confetti({
        particleCount: 100,
        spread: 70,
        origin: { y: 0.6 },
      });;
}



