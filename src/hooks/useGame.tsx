import { db } from "@apps/api/config/firebase";
import { namingCollection } from "@apps/api/names-collection";
import { authContextType } from "@apps/context/AuthContextProvider";
import { doc, getDoc, onSnapshot, updateDoc } from "firebase/firestore";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { useTimeout } from "./useTimeOut";

export const Moves = {
  ROCK: "ROCK",
  PAPPER: "PAPPER",
  SCISSORS: "SCISSORS",
};

export default function useGame(docId: string, user: authContextType["user"]) {
  const [participant1Move, setParticipant1Move] = useState<string | null>();
  const [participant2Move, setParticipant2Move] = useState<string | null>();
  const [hasMoveParticipant1, setHasMoveParticipant1] = useState<boolean | null>(false);
  const [hasMoveParticipant2, setHasMoveParticipant2] = useState<boolean | null>(false);
  const [winner, setWinner] = useState<string | null>();
  const [gameDocId, setGameDocId] = useState<string | null>(docId);

  const router = useRouter();

  const getWinner = async (move1: string, move2: string) => {

      if (move1 === move2) {
        setWinner("Empate");
      } else if ((move1 === Moves.ROCK && move2 === Moves.SCISSORS) || (move1 === Moves.PAPPER && move2 === Moves.ROCK) || (move1 === Moves.SCISSORS && move2 === Moves.PAPPER)) {
        setWinner("Player One");
      } else {
        setWinner("Player Two");
      }
    }
  

  const outRoom = async () => {
    try {
      if (gameDocId) {
        const roomDoc = doc(db, namingCollection.rooms, gameDocId);
        const roomSnapshot = await getDoc(roomDoc);
        const participants = roomSnapshot.get("participants");

        const newParticipants = participants.filter((participant: string) => {
          return participant !== user?.uid;
        });

        await updateDoc(roomDoc, { participants: newParticipants });
        router.back();
      } else {
        console.log("Document does not exist.");
      }
    } catch (error) {
      console.error("Error deleting array element:", error);
    }
  };

  const makeMove = async (move: string) => {
    if (gameDocId) {
      const roomDoc = doc(db, namingCollection.rooms, gameDocId);
      const roomSnapshot = await getDoc(roomDoc);
      const participants = roomSnapshot.get("participants");

      if (participants[0] === user?.uid && !hasMoveParticipant1) {
        setParticipant1Move(move);
        setHasMoveParticipant1(true);
        await updateDoc(roomDoc, {
          participant1Move: move,
        });
      } else if (participants[1] === user?.uid && !hasMoveParticipant2) {
        setParticipant2Move(move);
        setHasMoveParticipant2(true);
        await updateDoc(roomDoc, {
          participant2Move: move,
        });
      }
    }
  };

  useEffect(() => {
    if (gameDocId) {
      const gameDocRef = doc(db, "rooms", gameDocId);
      let timeOut: NodeJS.Timeout;
      const unsubscribe = onSnapshot(gameDocRef, (docSnapshot) => {
        const data = docSnapshot.data();
        if (data?.participant1Move && data?.participant2Move) {
          setParticipant1Move(data?.participant1Move);
          setParticipant2Move(data?.participant2Move);
          getWinner(data?.participant1Move, data?.participant2Move);
          setHasMoveParticipant1(false);
          setHasMoveParticipant2(false);

          timeOut = setTimeout( resetGame, 2000 )
        }
      });

      return () => {
        clearTimeout(timeOut)
        unsubscribe()
      };
    }
  }, [participant1Move,participant2Move]);

  const resetGame = async () => {
    if (gameDocId) {
      const roomDocRef = doc(db, namingCollection.rooms, gameDocId);

      setParticipant1Move(null);
      setParticipant2Move(null);
      setWinner(null);

      await updateDoc(roomDocRef, {
        participant1Move: "",
        participant2Move: "",
      });
    }
  };

  return {
    participant1Move,
    participant2Move,
    winner,
    makeMove,
    getWinner,
    hasMoveParticipant1,
    hasMoveParticipant2,
    outRoom,
  };
}
