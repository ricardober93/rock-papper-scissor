import { doc, getDoc, updateDoc, arrayUnion } from "firebase/firestore";

import { namingCollection } from "./names-collection";
import { db } from "./config/firebase";
export  const addParticipantToRoom = async (roomId: string, participantId: string) => {
  try {
    // Get the room document
    const roomDoc = doc(db, namingCollection.rooms, roomId);
    const roomSnapshot = await getDoc(roomDoc);
    const totalParticipant = roomSnapshot.get("participants")?.length
    

    if (roomSnapshot.exists()) {
      // Update the participants field in the room document
      await updateDoc(roomDoc, {
        participants: arrayUnion(participantId),
      });
      
      console.log("Participant added successfully", participantId, roomId);
    } else {
      console.log("Room does not exist");
    }
  } catch (error) {
    console.error("Error adding participant to room:", error);
    throw new Error("Failed to add participant to room");
  }
};
