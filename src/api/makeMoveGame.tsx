import { doc, getDoc, updateDoc } from "firebase/firestore";
import { db } from "./config/firebase";;

export const makeMove = async (roomId: string, participantId: string, move: string, getWinner: (move1: string, move2: string) => string) => {

  try {
    // Get the game state document
    const gameStateDoc = doc(db, "rooms", roomId, "gameState");
    const gameStateSnapshot = await getDoc(gameStateDoc);

    if (gameStateSnapshot.exists()) {
      const gameState = gameStateSnapshot.data();

      // Update the move for the participant
      const participantKey = `participant${participantId}`;
      await updateDoc(gameStateDoc, {
        [participantKey]: move,
      });

      // Check if both participants have made their moves
      if (gameState.participant1 && gameState.participant2) {
        const winner = getWinner(gameState.participant1, gameState.participant2);

        // Update the winner field in the game state document
        await updateDoc(gameStateDoc, {
          winner,
        });
      }

      console.log("Move made successfully");
    } else {
      console.log("Game state does not exist");
    }
  } catch (error) {
    console.error("Error making move:", error);
    throw new Error("Failed to make move");
  }
};
