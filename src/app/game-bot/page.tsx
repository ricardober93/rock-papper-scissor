"use client";
import Header from "@apps/components/Header";
import { useAuthContext } from "@apps/context/AuthContextProvider";
import { Showconfetti } from "@apps/helper/confetti";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";

export default function page() {
  const { user } = useAuthContext();
  const router = useRouter();

  const [options, _] = useState<string[]>(["ROCK", "PAPPER", "SCISSORS"]);

  const [playerOne, setPlayerOne] = useState<string>();
  const [playerBot, setPlayerBot] = useState<string>();
  const [winner, setWinner] = useState<string>();

  useEffect(() => {
    if (user == null) router.push("/login");
  }, [user]);

  const selectOption = (option: number) => {
    const value = options[option];
    setPlayerOne(value);
    botPlay();
  };

  const botPlay = () => {
    let value = options[Math.floor(Math.random() * 3)];
    setPlayerBot(value);
  };

  const gamePlayerVsBot = () => {
    if (playerOne === playerBot) {
      setWinner("EMPATE");
    } else if (playerOne === "ROCK") {
      if (playerBot === "PAPPER") {
        setWinner("PERDISTE");
      } else {
        setWinner("GANASTE");
        Showconfetti();
      }
    } else if (playerOne === "PAPPER") {
      if (playerBot === "SCISSORS") {
        setWinner("PERDISTE");
      } else {
        setWinner("GANASTE");
        Showconfetti();
      }
    } else if (playerOne === "SCISSORS") {
      if (playerBot === "ROCK") {
        setWinner("PERDISTE");
      } else {
        setWinner("GANASTE");
        Showconfetti();
      }
    }
  };

  const resetGame = () => {
    setPlayerOne("");
    setPlayerBot("");
    setWinner("");
  };

  useEffect(() => {
    resetGame();
  }, []);

  useEffect(() => {
    gamePlayerVsBot();
  }, [playerOne, playerBot]);

  return (
    <main className="flex min-h-screen flex-col items-center p-3">
      <Header photoUrl={user?.photoURL!} />

      <section className="flex-1 flex  flex-col h-full w-full justify-between items-center p-8">
         <article className="flex-grow h-full flex flex-col justify-center items-center">
         <div
          id="game-bot"
          className="h-full w-full flex flex-1 gap-3">
          <section className="w-1/2 flex flex-col justify-center items-center">
            <img
              className="w-32"
              src={`/images/${playerOne}-1.svg`}
              alt={playerOne}
            />
            {playerOne}
            <p className="text-lg text-center mx-auto"> Player One </p>
          </section>
          <section className="w-1/2 flex flex-col justify-center items-center">
            <img
              className="w-32"
              src={`/images/${playerBot}-2.svg`}
              alt={playerBot}
            />
            {playerBot}
            <p className="text-lg text-center mx-auto"> Player Bot </p>
          </section>
        </div>
        <section className="w-full flex justify-center items-center p-10">
          <h1 className="text-3xl text-yellow-600 font-bold">{winner}</h1>
        </section>
         </article>

        <section className="h- flex flex-col  justify-center items-center gap-3">
          <p> select one option:</p>

          <div className="w-full flex justify-center items-center gap-3">
            <button
              className="btn btn-neutral"
              onClick={() => selectOption(0)}>
              Rock
            </button>
            <button
              className="btn btn-primary"
              onClick={() => selectOption(1)}>
              Paper
            </button>
            <button
              className="btn btn-accent"
              onClick={() => selectOption(2)}>
              Scissors
            </button>
          </div>
        </section>
      </section>
    </main>
  );
}
