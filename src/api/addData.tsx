import { addDoc, collection, doc } from "firebase/firestore";
import { db } from "./config/firebase";

export default async function addData(colllectionName: string, data : {}) {
    let result = null;
    let error = null;

    try {
        result = await addDoc(collection(db, colllectionName), data,);
    } catch (e) {
        error = e;
    }

    return { result, error };
}