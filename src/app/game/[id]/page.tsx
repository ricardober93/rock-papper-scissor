"use client";

import Header from "@apps/components/Header";
import { useAuthContext } from "@apps/context/AuthContextProvider";
import useGame, { Moves } from "@apps/hooks/useGame";
import { useParams, useRouter } from "next/navigation";

export default function page() {
  const params = useParams();
  const { user } = useAuthContext();
  const { winner, makeMove, hasMoveParticipant1, hasMoveParticipant2, outRoom, participant1Move, participant2Move } = useGame(params.id, user);

  const selectOption = async (move: string) => {
    await makeMove(move);
  };

  const GoOutRoom = () => {
    outRoom();
  };

  return (
    <main className="flex min-h-screen flex-col items-center p-3">
      <Header photoUrl={user?.photoURL!} />

      <section className="flex-1 flex  flex-col h-full w-full justify-between items-center p-8">
        <article className="flex-grow h-full flex flex-col justify-center items-center">
          <div className="flex w-full justify-center items-center">
            <button
              className="btn btn-secondary"
              onClick={() => GoOutRoom()}>
              Salir de la Sala
            </button>
          </div>

          <div className="flex w-full justify-center items-center h-full flex-1 gap-5">
          <section className="w-1/2 flex flex-col justify-center items-center">
            {participant1Move ? (
              <img
                className="w-32"
                src={`/images/${participant1Move}-1.svg`}
                alt={participant1Move!}
              />
            ) : null}
            <p className="text-lg text-center mx-auto"> Player One </p>
          </section>
          <section className="w-1/2 flex flex-col justify-center items-center">
            {participant2Move ? (
              <img
                className="w-32"
                src={`/images/${participant2Move}-2.svg`}
                alt={participant2Move}
              />
            ) : null}
            <p className="text-lg text-center mx-auto"> Player Two </p>
          </section>
          </div>

          <div
            id="game-bot"
            className="h-full w-full flex flex-1 gap-3"></div>
          <section className="w-full flex justify-center items-center p-10">
            <h1 className="text-3xl text-yellow-600 font-bold">{winner}</h1>
          </section>
        </article>

        <section className="h- flex flex-col  justify-center items-center gap-3">
          <p> select one option:</p>

          <div className="w-full flex justify-center items-center gap-3">
            <button
              disabled={hasMoveParticipant1 || hasMoveParticipant2!}
              className="btn btn-neutral"
              onClick={() => selectOption(Moves.ROCK)}>
              Rock
            </button>
            <button
              disabled={hasMoveParticipant1 || hasMoveParticipant2!}
              className="btn btn-primary"
              onClick={() => selectOption(Moves.PAPPER)}>
              Paper
            </button>
            <button
              disabled={hasMoveParticipant1 || hasMoveParticipant2!}
              className="btn btn-accent"
              onClick={() => selectOption(Moves.SCISSORS)}>
              Scissors
            </button>
          </div>
        </section>
      </section>
    </main>
  );
}
