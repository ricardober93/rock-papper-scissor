import { auth } from "./config/firebase";

export default function useLogoutFirebase() {
  const logOut = async () => {
    try {
      await auth.signOut();
    } catch (e) {
      return e;
    }
  };

  return { logOut };
}
