"use client";

import login from "@apps/api/login";
import { useToast } from "@apps/context/ToastContextProvider";
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { SubmitHandler, useForm } from "react-hook-form";

type Inputs = {
  email: string,
  password: string,
};
export default function page() {
  const router = useRouter();
  const toast = useToast();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs>  =  async ({email, password}) => {
    const { result, error } = await login(email, password);


    if (error) {
     toast.error('Error al iniciar sesión')
  }
  toast.success("Bienvenido");
  return router.push("/game")
  }
  return (
    <main className="flex min-h-screen flex-col items-center justify-center p-24">
      <form className="grid grid-cols-1 items-center gap-4" onSubmit={handleSubmit(onSubmit)}>
        <input
          className="input w-full max-w-xs"
          type="email"
          placeholder="email@user.com"
          {...register("email")}
        />

        <input
        className="input w-full max-w-xs"
        type="password"
        placeholder="****"
        {...register("password", { required: true })} />
        {errors.password?.type === "required" && <span className="text-red-500">This field is required</span>}

        <button className="btn btn-primary" type="submit" > Iniciar Sesión </button>
      </form>
      <span>
        ¿No tienes una cuenta?{" "}
        <Link className="link link-primary" href="/create-user">Registrate</Link>
      </span>
    </main>
  );
}
