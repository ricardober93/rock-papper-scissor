"use client";
import addData from "@apps/api/addData";
import { addParticipantToRoom } from "@apps/api/addParticipantToRoom";
import { db } from "@apps/api/config/firebase";
import { namingCollection } from "@apps/api/names-collection";
import Header from "@apps/components/Header";
import { useAuthContext } from "@apps/context/AuthContextProvider";
import { collection } from "firebase/firestore";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { useCollection } from "react-firebase-hooks/firestore";
export default function page() {
  const { user } = useAuthContext();
  const router = useRouter();
  

  const [value, loading, error] = useCollection(collection(db, "rooms"), {
    snapshotListenOptions: { includeMetadataChanges: true },
  });

  const createRoom = async () => {
    const { result, error } = await addData(namingCollection.rooms, { name: "room1", description: "Sala para jugar" });
    if (error) {
      console.log(error);
    }
    console.log(result);
  };

  const playWithABot = () => {
    router.push(`/game-bot`);
  };

  const goToRoom = async (id: string) => {
    await addParticipantToRoom(id, user?.uid!);
    router.push(`/game/${id}`);
  };
  useEffect(() => {
    if (user == null) router.push("/login");
  }, [user]);

  return (
    <main className="flex min-h-screen flex-col items-center p-3">
      <Header photoUrl={user?.photoURL!} />
      <div className="alert alert-info flex justify-center items-center my-3">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="stroke-current shrink-0 h-6 w-6"
            fill="none"
            viewBox="0 0 24 24">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
            />
          </svg>
          <span>Versions Alfa!! Multplayer</span>
        </div>

      <section className="flex w-full justify-center items-center p-8">
        <button
          onClick={playWithABot}
          className="btn btn-secondary w-auto mx-auto">
          juegar con un bot
        </button>
      </section>

      <section className="grid w-full grid-cols-1 gap-4 justify-center items-center">
        {value?.docs?.length! > 0 ? (
          value?.docs?.map((doc) => (
            <div
              className="flex  gap-3 justify-center items-centers w-full text-center p-4"
              key={doc.id}>
              <h1 className="text-xl font-bold"> {doc.data().name}</h1>
              <button
                disabled={doc.data().participants?.length! >= 2}
                onClick={() => goToRoom(doc.id)}
                className="btn btn-primary btn-xs ">
                Entrar a la sala
              </button>
              {
                doc.data().participants?.length! >= 2 ? (
                  <div className="badge badge-secondary badge-outline"> Sala llena </div>
                ): (
                  null
                )
              }
            </div>
          ))
        ) : (
          <p className="w-full text-center"> no hay salas creadas</p>
        )}
        
      </section>
    </main>
  );
}
